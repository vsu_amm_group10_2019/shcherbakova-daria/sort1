﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sort
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.RowCount = 1;
            dataGridView1.ColumnCount = 10;
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
            {
                dataGridView1.Columns[i].HeaderCell.Value = (i + 1).ToString();
            }
            label4.Visible = false;
            label5.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                for (int j = 0; j < dataGridView1.Rows.Count; j++)
                {
                    dataGridView1.Rows[j].Cells[i].Value = null;
                }
            }
            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                for (int j = 0; j < dataGridView1.Rows.Count; j++)
                {
                    dataGridView1.Rows[j].Cells[i].Value = r.Next(int.Parse(textBox1.Text), int.Parse(textBox2.Text));
                    dataGridView1.Refresh();
                    Thread.Sleep(50);
                }
            }

            //Сортировка
            int temp;
            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                for (int j = i + 1; j < dataGridView1.Columns.Count; j++)
                {
                    if (int.Parse(dataGridView1.Rows[0].Cells[i].Value.ToString()) > int.Parse(dataGridView1.Rows[0].Cells[j].Value.ToString()))
                    {
                        dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.DarkRed;
                        dataGridView1.Rows[0].Cells[j].Style.BackColor = Color.DarkRed;
                        dataGridView1.Rows[0].Cells[i].Style.ForeColor = Color.White;
                        dataGridView1.Rows[0].Cells[j].Style.ForeColor = Color.White;
                        temp = int.Parse(dataGridView1.Rows[0].Cells[i].Value.ToString());
                        dataGridView1.Rows[0].Cells[i].Value = dataGridView1.Rows[0].Cells[j].Value;
                        dataGridView1.Rows[0].Cells[j].Value  = temp;
                        dataGridView1.Refresh();
                        Thread.Sleep(300);
                        dataGridView1.Rows[0].Cells[i].Style.BackColor = Color.White;
                        dataGridView1.Rows[0].Cells[j].Style.BackColor = Color.White;
                        dataGridView1.Rows[0].Cells[i].Style.ForeColor = Color.Black;
                        dataGridView1.Rows[0].Cells[j].Style.ForeColor = Color.Black;

                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //построение графика
            Graf data = new Graf();

            this.chart1.Series[0].Points.Clear();
            int x = 0, y = 0, b = 1000;
            while (x < b)
            {
                y = data.BubbleSort(data.CreateArray(x), "compare");
                this.chart1.Series[0].Points.AddXY(x, y);
                y = data.BubbleSort(data.CreateArray(x), "swap");
                this.chart2.Series[0].Points.AddXY(x, y);
                x++;
            }
        }
    }
}
