﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort
{
    class Graf
    {
        public int BubbleSort(int[] mas, string str)
        {
            int countCompare = 0;
            int countSwap = 0;           
            int temp;
            for (int i = 0; i < mas.Length; i++)
            {
                for (int j = i + 1; j < mas.Length; j++)
                {
                    countCompare++;
                    if (mas[i] > mas[j])
                    {
                        temp = mas[i];
                        mas[i] = mas[j];
                        mas[j] = temp;
                        countSwap++;
                    }
                }
            }
            if (str == "compare")
            {
                return countCompare;
            }
            else if (str == "swap")
            {
                return countSwap;
            }
            else
            {
                return 0;
            }
        }
     
        public int[] CreateArray(int length)
        {
            var array = new int[length];
            Random rand = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rand.Next(1, length + 1);
            }
            return array;
        }
    }
}
